@extends('layouts.guest-layout')
@section('content')
<div class="container-fluid relative">
    <div class="row">
        <div class="col-12 col-lg-5 our-company-header-text margin-from-menu">
            <div class="row justify-content-center">
                <div class="col-10 ">
                    <h1 class="big-header">For those who know that software is important</h1>
                    <div class="white-line"></div>
                    <h3 class="little-header-text">We help CTOs and Product Owners who need
                        a long-term software development partner
                    </h3>
                    <a href="" class="more-button">Read more</a>
                </div>

            </div>
        </div>
        <div class="d-none d-lg-block col-12 col-lg-7 our-company-background-side  margin-from-menu">
            <img class="big-image" src="{{asset('images/top-header-it-man.png')}}">
        </div>


    </div>
</div>

<div class="container-fluid our-offer-section">
    <div class="row margin-des-main">
       
        <div class="col-12 col-md-5 col-xl-6 d-none d-lg-block">
            <img class="horse-image" src="{{asset('images/horse2.png')}}">
            <div class="horse-overlay pulse">  
                    <h3>Mobile development</h3>
                    <div class="divider-horse"></div>
                    Native iOS and Android apps, plus cross-platform React Native development </br><a class="more-info-horse" href="">Read More...</a></div>
                    
            <div class="horse-overlay2 pulse">  <h3>Mobile development</h3>
                <div class="divider-horse"></div>
                Native iOS and Android apps, plus cross-platform React Native development  </br><a class="more-info-horse" href="">Read More...</a> </div>
            <div class="horse-overlay3 pulse">  <h3>Mobile development</h3>
                <div class="divider-horse"></div>
                Native iOS and Android apps, plus cross-platform React Native development  </br><a class="more-info-horse" href="">Read More...</a> </div>
            <div class="horse-overlay4 pulse">  <h3>Mobile development</h3>
                <div class="divider-horse"></div>
                Native iOS and Android apps, plus cross-platform React Native development  </br><a class="more-info-horse" href="">Read More...</a></div>
          
          
        </div>
            <div class="col-12 col-lg-7 col-xl-6">
                <div class="row ">
                    <div class="col-12">
                        <div class="our-offer-description margin">
                            <h4>Hire an agile development team
                                or let us build your product from scratch</h4>
                            <p>We support CTOs who need agile teams of skilled developers.
                                Hire a dedicated team or let us build your product from scratch.</p>
                                  <a href="" class="more-button">Read more</a>
                        </div>
                    </div>
                    
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/laravel-logo.png')}}">
                                <h5>Laravel</h5>
                            </div>
                            <div class="flip-card-back">
                                <span class="flip-main-text">Don’t hire just any Laravel developers</span>
                                <div class="divider-flip"></div>
                                Choose the best to benefit from Laravel’s full potential
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/wordpress-logo.png')}}">
                                <h5>Wordpress</h5>
                            </div>
                            <div class="flip-card-back">
                                Tekst na obrocie
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/php-logo.png')}}">
                                <h5>PHP</h5>
                            </div>
                            <div class="flip-card-back">
                                Tekst na obrocie
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/web-dev.jpg')}}">
                                <h5>Web Dev</h5>
                            </div>
                            <div class="flip-card-back">
                                Tekst na obrocie
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/e-com-logo.png')}}">
                                <h5>e-commerce</h5>
                            </div>
                            <div class="flip-card-back">
                                Tekst na obrocie
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 flip-card">
                        <div class=" our-offer-single-field margin flip-card-inner">
                            <div class="our-offer-single-field flip-card-front"><img
                                    src="{{asset('images/react.svg')}}">
                                <h5>React</h5>
                            </div>
                            <div class="flip-card-back">
                                Soon
                            </div>
                        </div>
                    </div>
    
                </div>
    
            </div>
        </div>
    </div>
</div>





<!-- Header section with image, text and button ends here -->
<div class="container-fluid our-portfolio d-none d-lg-block" id="our-portfolio">
    <div class="row blog">
        <div class="col-12">
                
            <div class="row">
            <div class="col-8">
                   
            <div id="container">
<div id="perspective">
                    <div id="carousel">
                        <figure>
                            <div class="logo"><img class="slider-image" src="{{asset('images/blog-1.jpg')}}">
                                <h3>What makes a good CTO great? Solving the unsolvable of course!  </h3>
                            </div>
                           <div class="blog-short-desc"> 
                             
                               <h4>We value your privacy
                                    We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse .
                               </br></br><span class="tags-blog">tags |</span> Test , Test , Test    
                                </h4>
                              

                               </div>
                            <div class="bottom-of-figure">
                                   
                                <div class="left-side">Category</div>
                                <div class="right-side">Read More</div>
                            </div>
                        </figure>
                        <figure>
                                <div class="logo"><img class="slider-image" src="{{asset('images/blog-1.jpg')}}">
                                    <h3>What makes a good CTO great? Solving the unsolvable of course!  </h3>
                                </div>
                               <div class="blog-short-desc"> 
                                 
                                   <h4>We value your privacy
                                        We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse .
                                   </br></br><span class="tags-blog">tags |</span> Test , Test , Test    
                                    </h4>
                                  
    
                                   </div>
                                <div class="bottom-of-figure">
                                       
                                    <div class="left-side">Category</div>
                                    <div class="right-side">Read More</div>
                                </div>
                            </figure>
                            <figure>
                                    <div class="logo"><img class="slider-image" src="{{asset('images/blog-1.jpg')}}">
                                        <h3>What makes a good CTO great? Solving the unsolvable of course!  </h3>
                                    </div>
                                   <div class="blog-short-desc"> 
                                     
                                       <h4>We value your privacy
                                            We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse .
                                       </br></br><span class="tags-blog">tags |</span> Test , Test , Test    
                                        </h4>
                                      
        
                                       </div>
                                    <div class="bottom-of-figure">
                                           
                                        <div class="left-side">Category</div>
                                        <div class="right-side">Read More</div>
                                    </div>
                                </figure>
                                <figure>
                                        <div class="logo"><img class="slider-image" src="{{asset('images/blog-1.jpg')}}">
                                            <h3>What makes a good CTO great? Solving the unsolvable of course!  </h3>
                                        </div>
                                       <div class="blog-short-desc"> 
                                         
                                           <h4>We value your privacy
                                                We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse .
                                           </br></br><span class="tags-blog">tags |</span> Test , Test , Test    
                                            </h4>
                                          
            
                                           </div>
                                        <div class="bottom-of-figure">
                                               
                                            <div class="left-side">Category</div>
                                            <div class="right-side">Read More</div>
                                        </div>
                                    </figure>
                                    <figure>
                                            <div class="logo"><img class="slider-image" src="{{asset('images/blog-1.jpg')}}">
                                                <h3>What makes a good CTO great? Solving the unsolvable of course!  </h3>
                                            </div>
                                           <div class="blog-short-desc"> 
                                             
                                               <h4>We value your privacy
                                                    We and our partners use technology such as cookies on our site to personalise content and ads, provide social media features, and analyse .
                                               </br></br><span class="tags-blog">tags |</span> Test , Test , Test    
                                                </h4>
                                              
                
                                               </div>
                                            <div class="bottom-of-figure">
                                                   
                                                <div class="left-side">Category</div>
                                                <div class="right-side">Read More</div>
                                            </div>
                                        </figure>
                       
                     

                    </div>
                    
                </div>
             

            </div>
          
        </div>
        <div class="col-4">
                <div class="right-blog-box"> 
                    <h1>From our dev's with love</h1>
                    <p>We help CTOs and Product Owners who need
                        a long-term software development partner
                    </p></div>
                    <a href="" class="more-button">Read more</a>
                </div>
       
        </div>
    </div>
    </div>
</div>

<div class="container-fluid our-portfolio-mobile d-lg-none">
    <div class="row justify-content-center">
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider1.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Aplikacja</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider2.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Aplikacja</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider3.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Portal</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="little-logo" src="{{asset('images/slider4.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Współpraca</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider5.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">e-commerce</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider6.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Strona</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider7.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Strona</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider8.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Strona</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider9.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">e-commerce</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider10.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Współpraca</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider11.jpg')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Strona</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider12.jpg')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">e-commerce</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider13.jpg')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">Współpraca</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
        <div class="col-10 col-sm-5">
            <div class="single-project">
                <div class="logo">
                    <img class="" src="{{asset('images/slider14.png')}}">
                </div>
                <div class="bottom-of-figure">
                    <div class="left-side">e-commerce</div>
                    <div class="right-side">Zobacz</div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="nowasekcja" style="margin-top:100px; margin-bottom:100px;">
    <div class="container">
        <div class="befor-fotter-heading"><h3>Best Solution for you project <span class="addidtion-text">Choose the best way for your succes</span><div class="divider"></div><div class="divider2"></div><div class="divider3"></div><a href="" class="more-button">Read more</a></h3></div>
        <div class="button-right-side"> </div>
        <div class="row section-4">
          <div class="col-sm">
            <div class="media">
                <img src="{{asset('images/laravel-logo.png')}}"  alt="...">
                <div class="media-body">
                  <h5 class="mt-0">Media heading</h5>
                  <div class="divider"></div>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
              </div>
          </div>
          <div class="col-sm">
            <div class="media">
                <img src="{{asset('images/php-logo.png')}}"  alt="...">
                <div class="media-body">
                  <h5 class="mt-0">Media heading</h5>
                  <div class="divider"></div>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
              </div>
          </div>
          <div class="col-sm">
            <div class="media">
                <img src="{{asset('images/js-logo.png')}}"  alt="...">
                <div class="media-body">
                  <h5 class="mt-0">Media heading</h5>
                  <div class="divider"></div>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
              </div>
          </div>
        </div>
      </div>



</div>


<div class="container-fluid d-lg-none mobile-contact-section">
    <div class="row">
        <div class="col-12 col-sm-6 mobile-address">
            <h3>SIEDZIBA FIRMY:</h3>
            Sienkiewicza 39<br />
            63-300 Pleszew<br />
            Pierwsze Piętro<br /><br />
            <h3>GODZINY OTWARCIA:</h3>
            8:00 - 16:00<br />
            <i class="fa fa-phone" style="font-size:20px"></i> 570 570 509<br />
            <i class="fa fa-envelope" aria-hidden="true" style="font-size: 20px;"></i>
            kontakt@mirit.pl
        </div>
        <div class="col-12 col-sm-6 contact-form-field mobile-contact-form">
            <h3>Napisz do nas!</h3>
            <span class="contact-form-description">Imię</span>
            <input type="text" class="contact-form-input">
            <span class="contact-form-description">Email</span>
            <input type="email" class="contact-form-input">
            <span class="contact-form-description">Wiadomość</span>
            <textarea rows="5"></textarea>
            <input type="submit" value="Wyślij" class="btn-block send-button button-animation">
        </div>
        <div class="col-12 mobile-map"> <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2462.287542143325!2d17.783520515751675!3d51.892216479700245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47052dc715310dc9%3A0x5af576284a41efbc!2sSienkiewicza+39%2C+63-300+Pleszew!5e0!3m2!1spl!2spl!4v1564560231640!5m2!1spl!2spl"
                height="250px" frameborder="0" allowfullscreen></iframe></div>
    </div>
</div>

<div class="container-fluid d-none d-lg-block" id="contact">
    <div class="row">
        <div class="col-12 contact-section-background"></div>
        <div class="col-6 relative map">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2462.287542143325!2d17.783520515751675!3d51.892216479700245!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47052dc715310dc9%3A0x5af576284a41efbc!2sSienkiewicza+39%2C+63-300+Pleszew!5e0!3m2!1spl!2spl!4v1564560231640!5m2!1spl!2spl"
                class="remove-margin" width="100%" height="500px" frameborder="0"
                style="border:0; padding: 0; margin: 0;" allowfullscreen></iframe>

        </div>
        <div class="col-6 contact-form">
            <div class="contact-form-field contact-form-field-position">
                <h3>Napisz do nas!</h3>
                <span class="contact-form-description">Imię</span>
                <input type="text" class="contact-form-input">
                <span class="contact-form-description">Email</span>
                <input type="email" class="contact-form-input">
                <span class="contact-form-description">Wiadomość</span>
                <textarea rows="10"></textarea>
                <input type="submit" value="Wyślij" class=" send-button button-animation">
            </div>
            <div class="row justify-content-end">
                <div class="col-4 addres">
                    <span class="strong">SIEDZIBA FIRMY:</span><br /><br />
                    Sienkiewicza 39<br />
                    63-300 Pleszew<br />
                    Pierwsze Piętro<br /><br />
                    <span class="strong">GODZINY OTWARCIA:</span><br /><br />
                    8:00 - 16:00<br />
                    <i class="fa fa-phone" style="font-size:20px"></i> 570 570 509<br />
                    <i class="fa fa-envelope" aria-hidden="true" style="font-size: 20px;"></i>
                    kontakt@mirit.pl
                </div>
            </div>
        </div>
    </div>
</div>
<a id="back-to-top" href="#" class="btn back-to-top-button btn-lg back-to-top" role="button"
    title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span
        class="glyphicon glyphicon-chevron-up">
        <svg width="35px" height="35px" fill="white" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612.002 612.002"
            style="enable-background:new 0 0 612.002 612.002;" xml:space="preserve">
            <g>
                <g>
                    <path d="M592.639,358.376L423.706,189.439c-64.904-64.892-170.509-64.892-235.421,0.005L19.363,358.379
			c-25.817,25.817-25.817,67.674,0,93.489c25.817,25.817,67.679,25.819,93.491-0.002l168.92-168.927
			c13.354-13.357,35.092-13.361,48.444-0.005l168.93,168.932c12.91,12.907,29.825,19.365,46.747,19.365
			c16.915,0,33.835-6.455,46.747-19.365C618.456,426.051,618.456,384.193,592.639,358.376z" />
                </g>
        </svg>
    </span>
</a>
@endsection
