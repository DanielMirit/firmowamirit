<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class=" lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class=" lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class=" lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="pl">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Mirit Digital Agency</title>
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/my-style.min.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link href="https://fonts.googleapis.com/css?family=Audiowide&display=swap&subset=latin-ext" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    
    
    
   
    

    <script src="{{asset('js/3dcarousel.js')}}">
    </script>
  

    <script type="text/javascript">
        $(document).ready(function () {
            $('#carousel').carousel3d();
        });

    </script>
    <script src="{{asset('js/my-js.js')}}">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Barlow+Semi+Condensed&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<body>

    <!-- Header section with image, text and button starts here -->
    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <div class="header p-4 text-right">
                <h4 class="text-white">Collapsed content</h4>
                <span class="text-muted">
                    
                    <form>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Email address</label>
                          <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                        </div>
                        <div class="form-group form-check">
                          <input type="checkbox" class="form-check-input" id="exampleCheck1">
                          <label class="form-check-label" for="exampleCheck1">Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>




                </span>
            </div>
        </div>
        <nav class="navbar navbar-dark header relative">
            <img class="circle-logo" src="{{asset('images/circle-logo.png')}}">
            <span class="company-name">M I R I T <span class="company-surname">| software company</span> </span>
           
              
            <ul class="nav navcustom">
                <li class="nav-item">
                  <a class="nav-link active" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Resources</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                
              </ul>

            <button class="navbar-toggler  hamburger-circle" type="button" data-toggle="collapse"
                data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                aria-expanded="false" aria-label="Toggle navigation" style="width: 230px;
                top: 26px;">
              <span class="estproject">Estimate your project</span>
            </button>
        </nav>
    </div>
    @yield('content')

    <div class="footer container-fluid">
        <div class="row">
            <div class="col-12 col-lg-10 footer-color-background">
                <div class="row">
                    <div class="col-1 d-none d-lg-block"></div>
                    <div class="col-6 col-lg-2 footer-section-one">
                        <h4>O nas</h4>
                        <p>Jesteśmy pasjonatami tego co robimy, nikt nigdy do niczego nas nie zmuszał. Wszystko
                            robimy z
                            pasji dla idei dążenia do pomagania innym w rozwoju ich przedsiębiorstw. Strony
                            internetowe,
                            które tworzymy już od ponad 8 lat oraz reklamy, które kreujemy docierają do klientów
                            firm, z
                            którymi współpracujemy od lat.</p>
                    </div>
                    <div class="col-6 col-lg-2 footer-section-two">
                        <h4>Nawigacja</h4>
                        <ul>
                            <a href="">
                                <li class="menu-list">Adres#1</li>
                            </a>
                            <a href="">
                                <li class="menu-list">Adres#2</li>
                            </a>
                            <a href="">
                                <li class="menu-list">Adres#3</li>
                            </a>
                            <a href="">
                                <li class="menu-list">Adres#4</li>
                            </a>
                            <a href="">
                                <li class="menu-list">Adres#5</li>
                            </a>
                            <ul>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center subfooter"><span class="made-by">
                    <script type="text/javascript">
                        var year = new Date();
                        document.write(year.getFullYear());

                    </script>
                    &copy Wykonanie MIRIT
                </span> <img src="{{asset('images/insta-icon.png')}}" alt="" class="insta-image"> <img
                    src="{{asset('images/fb-icon.png')}}" alt="" class="fb-image"></div>
        </div>

    </div>
</body>

</html>
